# GIS Application

## Virtualenv configuration
Create a virtualenv:
 ```
git clone https://gitlab.com/scots4ever/gis.git
cd gis
virtualenv --python=`python3.6` env
source env/bin/activate
pip install -r requirements.txt
pip install -e .
```
## Database configuration and data importation
This project uses a Postgis Docker. You need to install docker and docker-compose. 
When they have been installed, you need execute:
```
docker-compose build
docker-compose run -d
```
Time to initialize database.
```
python gis/manage.py db_init

Droping collections...
Tables not created yet...
Droping types...
Creating types...
Create postal_code table schema...
Create pstats schema...
Type age_range created...
Type gender created...
Created paystats table

```
We are going to import data inside tables. Two variables has been added in this commands in order
to let you export csvs paths.
```
python gis/manage.py import_postal_codes $POSTAL_CODES_CSV_PATH
python gis/manage.py import_pstats $PAYSTATS_CSV_PATH
```
Final steps:
```
cd ..
export APP_SETTINGS_MODULE=gis.config.local.Config
export FLASK_APP=gis/main.py
flask run
```