# System imports
# Third party imports
import pandas as pd
# Local imports
from .persistency import DBManager

POSTAL_CODES_TABLE = 'postal_codes'
PAYSTATS_TABLE = 'paystats'


def drop_types():
    drop_types = """
        DROP TYPE IF EXISTS age_range;
        DROP TYPE IF EXISTS gender;
    """
    DBManager.execute_query(drop_types)


def drop_tables():
    delete_tables_query = """
        TRUNCATE TABLE {postal_code_table};
        DROP TABLE {postal_code_table};
        TRUNCATE TABLE {paystats_table};
        DROP TABLE {paystats_table};
    """.format(
        postal_code_table=POSTAL_CODES_TABLE,
        paystats_table=PAYSTATS_TABLE
    )
    DBManager.execute_query(delete_tables_query)


def create_postal_codes_table():
    query = """
    CREATE TABLE IF NOT EXISTS {table}( 
    the_geom geometry, 
    code integer, 
    id integer)
    """.format(
        table=POSTAL_CODES_TABLE
    )
    DBManager.execute_query(
        query=query
    )


def create_paystats_table():
    range_type_query = """
    DROP TYPE IF EXISTS age_range;
    CREATE TYPE age_range AS ENUM(
        '<=24',
        '25-34',
        '35-44',
        '55-64',
        '45-54',
        '>=65'
    );
    """
    DBManager.execute_query(range_type_query)
    print('Type age_range created...')

    gender_type_query = """
    DROP TYPE IF EXISTS gender;
    CREATE TYPE gender AS ENUM(
        'M', 'F'
    );
    """
    DBManager.execute_query(gender_type_query)
    print('Type gender created...')

    table_creation_query = """
    CREATE TABLE IF NOT EXISTS {table}( 
        amount DOUBLE precision ,
        p_month  DATE,
        p_age age_range,
        p_gender gender,
        postal_code_id INTEGER,
        id INTEGER PRIMARY KEY
    )
    """.format(
        table=PAYSTATS_TABLE,
    )
    DBManager.execute_query(table_creation_query)
    print('Created {} table'.format(PAYSTATS_TABLE))


def create_postal_codes_table():
    query = """
    CREATE TABLE IF NOT EXISTS {table}( 
        the_geom geometry, 
        code integer, 
        id integer
    )
    """.format(
        table=POSTAL_CODES_TABLE
    )
    DBManager.execute_query(
        query=query
    )


def import_postal_codes_data(file_path):

    df = pd.read_csv(file_path)
    df_columns = list(df)

    # create (col1,col2,...)
    columns = ",".join(df_columns)

    # create VALUES('%s', '%s",...) one '%s' per column
    values = "VALUES({})".format(",".join(["%s" for _ in df_columns]))

    # create INSERT INTO table (columns) VALUES('%s',...)
    insert_stmt = """
    INSERT INTO {} ({}) {}
    """.format(
        POSTAL_CODES_TABLE,
        columns,
        values,
    )
    if values:
        truncate_query = "TRUNCATE TABLE {table}; DELETE FROM {table}".format(
            table=POSTAL_CODES_TABLE
        )
        DBManager.execute_query(query=truncate_query)
    DBManager.execute_query(
        values=df.values.tolist(),
        query=insert_stmt,
    )


def import_pstats_data(file_path):
    df = pd.read_csv(file_path)
    df_columns = list(df)

    columns = ','.join(df_columns)
    values = 'VALUES({})'.format(','.join(['%s' for _ in df_columns]))

    insert_stmt = """
    INSERT INTO {} ({}) {}
    """.format(
        PAYSTATS_TABLE,
        columns,
        values
    )
    if values:
        truncate_query = "TRUNCATE TABLE {table}; DELETE FROM {table}".format(
            table=PAYSTATS_TABLE
        )
        DBManager.execute_query(query=truncate_query)
    DBManager.execute_query(
        query=insert_stmt,
        values=df.values.tolist(),
    )
