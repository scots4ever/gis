# System imports
import os
# Third party imports
from psycopg2.pool import SimpleConnectionPool
from flask import Flask

_app = None
_db_pool = None


def create_app():
    global _app
    if not _app:
        _app = Flask('gis')
        _app.url_map.strict_slashes = False
        config_class = os.getenv('APP_SETTINGS_MODULE')
        _app.config.from_object(config_class)
    return _app


def get_conn_pool():
    global _db_pool
    app = create_app()
    if not _db_pool:
        DSN = 'host={host} dbname={db} user={user} password={password}'.format(
            host=app.config['DB_HOST'],
            user=app.config['DB_USER'],
            password=app.config['DB_PASSWORD'],
            db=app.config['DB_NAME'],
        )
        _db_pool = SimpleConnectionPool(
            minconn=1,
            maxconn=100,
            dsn=DSN
        )
    return _db_pool
