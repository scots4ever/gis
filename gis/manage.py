# System imports
import os
# Third party imports
from flask import Flask
from flask_script import Manager
# Local imports
from gis.dbsetup import (drop_types,
                         drop_tables,
                         create_paystats_table,
                         create_postal_codes_table,
                         import_postal_codes_data,
                         import_pstats_data,)

# configure your app
app = Flask('gis')
app.url_map.strict_slashes = False
config_class = os.getenv('APP_SETTINGS_MODULE')
app.config.from_object(config_class)

manager = Manager(app)


@manager.command
def db_init():
    print('Droping collections...')
    try:
        drop_tables()
    except:
        print('Tables not created yet...')
    print('Droping types...')
    try:
        drop_types()
    except:
        print('Types doesn\'t defined yet')
    print('Creating types...')

    print('Create postal_code table schema...')
    create_postal_codes_table()
    print('Create pstats schema...')
    create_paystats_table()


@manager.command
@manager.option('-f', '--file_name', dest='file', help='CSV file')
def import_postal_codes(file):

    print('Importing postal codes CSV...')
    import_postal_codes_data(file_path=file)


@manager.command
@manager.option('-f', '--file_name', dest='file', help='CSV file')
def import_pstats(file):
    print('Importing payment stats CSV...')
    import_pstats_data(file_path=file)


if __name__ == '__main__':
    manager.run()
