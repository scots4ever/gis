from ..factories import get_conn_pool


class Region:

    @classmethod
    def retrieve_regions(cls, point_a, point_b):
        pool = get_conn_pool()
