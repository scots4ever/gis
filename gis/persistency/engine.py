# System imports
# Third party imports
from psycopg2.extras import RealDictCursor
# Local imports
from ..factories import get_conn_pool


class DBManager:
    """Class to manage database.

    Will contain code for manage connections, build pools and managing
    operations."""

    MIN_CONN = 1
    MAX_CONN = 50

    @staticmethod
    def execute_query(query, values=None):
        psql_pool = get_conn_pool()
        conn = psql_pool.getconn()
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        args = [
            query,
        ]
        if values:
            args.append(values)
            cursor.executemany(*args)
        else:
            cursor.execute(*args)
        conn.commit()
        return cursor
