from flask import (make_response,
                   jsonify,
                   request, )
import json

from .factories import create_app
from .business_logic import Geometry

app = create_app()


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/stats/total')
def retrieve_total():
    """Returns total turnover amount by two given dates."""
    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')
    # TODO: must be imlpemented
    return {
        'total': 476494732.23
    }


@app.route('/geometry/stats/all')
def retrieve_stats_within_dates():
    """Retrieve stats by geometry between two given dates."""
    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')
    all_geometries = Geometry.retrieve_all_areas_stats(
        from_date, to_date
    )
    response = {'geometries': all_geometries}
    return make_response(jsonify(response))


@app.route('/geometry/stats/all/byMonth')
def retrieve_stats_timeseries():
    """Retrieve stats adding time dimension."""
    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')
    all_geometries = Geometry.retrieve_all_areas_stats_by_month(
        from_date, to_date
    )
    response = {'geometries': all_geometries}
    return make_response(jsonify(response))


@app.route('/geometry/search')
def retrieve_geometry_in_square():
    """This endpoint manager will retrieve geometries ids inside square."""
    x = tuple(json.loads(request.args.get('x')))
    y = tuple(json.loads(request.args.get('y')))
    z = tuple(json.loads(request.args.get('z')))
    w = tuple(json.loads(request.args.get('w')))

    try:
        result = Geometry.retrieve_areas(x, y, z, w)

    except Exception as e:
        result = {
            'status': 'KO',
            'reason': repr(e)
        }
        raise

    return make_response(
        jsonify(result)
    )


@app.route('/filters/all')
def retrieve_filters():
    # TODO: database access will be needed
    return {
        'gender': ['M', 'F'],
        'age': [
            '<=24',
            '25-34',
            '35-44',
            '55-64',
            '45-54',
            '>=65',
        ]
    }
