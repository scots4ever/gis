# System imports
from datetime import datetime
import json
# Third party imports
from shapely.geometry import shape
# Local imports
from ..persistency import DBManager


class Geometry:
    POINT = 'Point'
    POLYGON = 'Polygon'
    DATE_FORMAT = '%Y-%m-%d'

    @staticmethod
    def build_polygon(points: list) -> object:
        """Build a polyon

        """
        first_point = points[0]
        last_point = points[-1]
        if first_point[0] != last_point[0] or first_point[1] != last_point[1]:
            raise Exception('Polygon must to be closed')
        polygon_dict = dict(
            type=Geometry.POLYGON,
            coordinates=[points, ],
            crs={"type": "name", "properties": {"name": "EPSG:4326"}}
        )
        return polygon_dict

    @staticmethod
    def valid_point(point: tuple) -> bool:
        valid = (
            len(point) == 2 and
            isinstance(point[0], float) and
            isinstance(point[1], float)
        )
        return valid

    @staticmethod
    def retrieve_areas(upper_left_point: tuple,
                       upper_right_point: tuple,
                       lower_left_point: tuple,
                       lower_right_point: tuple) -> list:
        """Retrieves list of areas inside a square build by x and y coords.

        The square will be build using a and b

        :param upper_left_point: upper left point of a square
        :type upper_left_point: tuple
        :param upper_right_point: upper right point of a square
        :type upper_right_point: tuple
        :param lower_left_point: lower right point of a square
        :type lower_left_point: tuple
        :param lower_right_point: lower right point of a square
        :type lower_right_point: tuple
        :return list of postal_codes_ids

        A valid polygon could be:
        >>>x=[-3.86444091796875,40.2863352647122]
        >>>y=[-3.6536407470703125,40.2863352647122]
        >>>z=[-3.6536407470703125,40.483515047963024]
        >>>w=[-3.86444091796875,40.483515047963024]
        >>> retrieve_areas(x, y, z, w)
        >>> [
        >>>  {
        >>>      "postal_code_id": 6179
        >>>  },
        >>>  {
        >>>      "postal_code_id": 6314
        >>>  }
        >>> ]
        """
        all_points = [
            upper_right_point,
            upper_left_point,
            lower_right_point,
            lower_left_point,
        ]

        are_all_points_valid = all(
            Geometry.valid_point(point)
            for point in all_points
        )

        if not are_all_points_valid:
            raise ValueError('Invalid arguments found: {}'.format(
                all_points
            ))

        polygon = Geometry.build_polygon(
            [
                upper_right_point,
                upper_left_point,
                lower_right_point,
                lower_left_point,
                # XXX: adds first as last one to close polygon
                upper_right_point,
            ]
        )

        query = """
        SELECT id as postal_code_id
        FROM postal_codes
        WHERE ST_Contains(ST_GeomFromGeoJSON('{polygon}'),
                          postal_codes.the_geom)
        """.format(polygon=json.dumps(polygon))

        try:
            cursor = DBManager.execute_query(query)
            result = [
                item
                for item in cursor
            ]
            return result
        except Exception as e:
            raise

    @staticmethod
    def retrieve_all_areas_stats_by_month(from_date: str, to_date: str):
        from_date_datetime = datetime.strptime(from_date, '%Y-%m-%d')
        to_date_datetime = datetime.strptime(to_date, '%Y-%m-%d')

        query = """
        select SUM(amount) agg_amount,
               paystats.postal_code_id as postal_code_id,
               postal_codes.code as postal_code,
               paystats.p_gender as gender,
               paystats.p_age as age,
               paystats.p_month as p_date
        from paystats
        join postal_codes
        on paystats.postal_code_id = postal_codes.id
        where paystats.p_month >= '{from_date}'::date 
              AND paystats.p_month <= '{to_date}'::date
        group by (paystats.postal_code_id,
              paystats.p_gender,
              paystats.p_age,
              paystats.p_month,
              postal_codes.code)
        order by agg_amount DESC
        """.format(
            from_date=from_date_datetime.strftime(Geometry.DATE_FORMAT),
            to_date=to_date_datetime.strftime(Geometry.DATE_FORMAT),
        )
        query_result = DBManager.execute_query(query)
        result = [
            {
                'gender': element['gender'],
                'age': element['age'],
                'amount': element['agg_amount'],
                'postal_code_id': element['postal_code_id'],
                'postal_code': element['postal_code'],
                'date': element['p_date']
            }
            for element in query_result
        ]
        return result


    @staticmethod
    def retrieve_all_areas_stats(from_date: datetime, to_date: datetime):
        from_date_datetime = datetime.strptime(from_date, '%Y-%m-%d')
        to_date_datetime = datetime.strptime(to_date, '%Y-%m-%d')

        query = """
        select SUM(amount) agg_amount,
               paystats.postal_code_id as postal_code_id,
               postal_codes.code as postal_code,
               paystats.p_gender as gender,
               paystats.p_age as age
        from paystats
        join postal_codes
        on paystats.postal_code_id = postal_codes.id
        where paystats.p_month >= '{from_date}'::date 
              AND paystats.p_month <= '{to_date}'::date
        group by (paystats.postal_code_id,
              paystats.p_gender,
              paystats.p_age,
              postal_codes.code)
        order by agg_amount DESC
        """.format(
            from_date=from_date_datetime.strftime(Geometry.DATE_FORMAT),
            to_date=to_date_datetime.strftime(Geometry.DATE_FORMAT),
        )
        query_result = DBManager.execute_query(query)
        result = [
            {
                'gender': element['gender'],
                'age': element['age'],
                'amount': element['agg_amount'],
                'postal_code_id': element['postal_code_id'],
                'postal_code': element['postal_code'],
            }
            for element in query_result
        ]
        return result
