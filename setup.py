from setuptools import setup, find_packages
from os import path
here = path.abspath(path.dirname(__file__))

setup(
    name='gis',
    version='1.0.0',

    description='A Gis Python project',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required

    python_requires='>=3.5.*',

    entry_points={
        'console_scripts': [
            'create_schemas=gis.dbsetup:create_schemas',
            'import_data=gis.dbsetup:import_data',
            'manage=gis.manage:run_manager',
        ],
    },
)
